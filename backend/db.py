import motor.motor_asyncio
from bson import ObjectId
from model import Todo

client = motor.motor_asyncio.AsyncIOMotorClient(
    "mongodb://localhost:27017"
)

db = client.TodoList
collection = db.todo

async def fetch_todo(_id):
    document = await collection.find_one({
        "_id": _id
    })
    return document

async def fetch_todos():
    todos = []
    cursor =  collection.find({})
    async for document in cursor:
        todos.append({
            "id": str(document["_id"]),
            "title": document["title"],
            "description": document["description"]
        })
    
    return todos

async def create_todo(todo):
    document = todo
    result = await collection.insert_one(document)

    return {
        "id": str(document["_id"]),
        "title": document["title"],
        "description": document["description"]
    }

async def update_todo(id, title, description):
    await collection.update_one({
        "_id": ObjectId(id),
    }, {
        "$set": {
            "title": title,
            "description": description
        }
    })

    document = await collection.find_one({
        "_id": ObjectId(id)
    })

    return document

async def remove_todo(id):
    collection.delete_one({
        "_id": ObjectId(id)
    })
    return True