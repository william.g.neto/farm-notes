from model import Todo
from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware

from db import (
    fetch_todo,
    fetch_todos,
    create_todo,
    update_todo,
    remove_todo
)
origins = ["*"]

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)

@app.get("/api/todo")
async def list_todo():
    response = await fetch_todos()

    return response


@app.get("/api/todo/{id}", response_model=Todo)
async def get_todo(id):
    response = await fetch_todo(id)

    if response:
        return response
    raise HTTPException(404, f"there is no Todo with this id {id}")

@app.post("/api/todo")
async def post_todo(todo:Todo):
    response = await create_todo(todo.__dict__)

    if response:
        return response
    raise HTTPException(400, "Bad request")

@app.put("/api/todo/{id}", response_model=Todo)
async def put_todo(id:str, description:str):
    response = await update_todo(
        id,
        description
    )
    
    if response:
        return response
    raise HTTPException(400, "Bad request")

@app.delete("/api/todo/{id}")
async def delete_todo(id):
    response = await remove_todo(id)

    if response:
        return response
    raise HTTPException(400, "Bad request")