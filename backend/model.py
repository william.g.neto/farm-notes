from pydantic import BaseModel
from bson.objectid import ObjectId as BsonObjectId

class Todo(BaseModel):
    title: str
    description: str